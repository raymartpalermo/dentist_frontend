<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_web_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->string('contact');
            $table->string('banner_image')->nullable();
            $table->string('logo')->nullable();
            $table->text('banner_text');
            $table->string('about_us_image')->nullable();
            $table->string('address');
            $table->text('about_us');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_web_information');
    }
}
