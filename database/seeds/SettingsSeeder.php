<?php

use App\Laravel\WebInformation;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WebInformation::create([
        	'banner_image' => '1580984417.png',
        	'logo'	=> '1580984587.png',
        	'about_us_image' => '1580984587.jpg',
        	'about_us' => 'edit about us',
        	'banner_text' => 'edit banner',
        	'address' => 'Edit address',
        ]);
    }
}
