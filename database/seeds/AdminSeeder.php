<?php

use Illuminate\Database\Seeder;
use App\Laravel\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	"firstname" => "Aldrin",
        	"lastname"	=>	"Dela Cruz",
        	"contact"	=> "09987654312",
        	"email"	=> "aldrin@mailtrap.com",
        	"type" => "doctor",
        	"age"	=> "30",
        	"address" => "Quezon City",
        	"password" => bcrypt("password"),
        ]);
        User::create([
            "firstname" => "Jerry",
            "lastname"  =>  "Agan",
            "contact"   => "0945678954",
            "email" => "jerry@mailtrap.com",
            "type" => "patient",
            "age"   => "23",
            "address" => "Quezon City",
            "password" => bcrypt("password"),
        ]);
    }
}
