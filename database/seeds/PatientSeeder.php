<?php

use Illuminate\Database\Seeder;
use App\Laravel\appointment;
class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        appointment::create([
        	"firstname" => "Jerry",
            "lastname"  =>  "Agan",
            "contact"   => "0945678954",
            "email" => "jerry@mailtrap.com",
 			"date" => "2020-2-2"
        ]);
    }
}
