<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $user = Auth::user()->type;
            switch($user)
            {
                case 'doctor':
                    return redirect()->route('system.dashboard');
                    break;

                case 'patient':
                    return redirect()->route('frontend.confirm_appointment');
                    break;
                default:
                    return abort(404);

            }
        }

        return $next($request);
    }
}
