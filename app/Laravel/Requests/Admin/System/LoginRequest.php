<?php

namespace App\Laravel\Request\Admin\System;

use Illuminate\Foundation\Http\FormRequest;


/**
 * 
 */
class LoginRequest extends FormRequest
{
	
	public function rules()
	{
		$rules = [
			'email' => 'required|email',
			'password' => 'required',
		];

		return $rules;
	}

	public function messages()
	{
		return [
			'email.required'	=> 'E-mail is required',
			'password.required' => 'Password is required',
		];
	}
}