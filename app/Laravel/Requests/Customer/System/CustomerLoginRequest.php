<?php

namespace App\Laravel\Request\Customer\System;

use Illuminate\Foundation\Http\FormRequest;


class CustoerLoginRequest extends FormRequest
{
	public function rules()
	{
			'firstname' => "required",
			'lastname'	=> "required",
			'contact' => "required",
			'email'	=> 'email|required|unique:users,email,id,NULL,deleted_at,NULL',
			'address' => 'required',
			'age'	=> 'required|unique',
	}
}