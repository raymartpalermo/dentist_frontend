<?php

namespace App\Laravel\Request\Customer\System;

use Illuminate\Foundation\Http\FormRequest;



class PatientSignUp extends FormRequest
{
	public function rules()
	{
		$rules = [
			'firstname' => 'required',
			'lastname' => 'required',
			'email'	=> 'email|required|unique:users',
			'password' => 'required',
			'age'	=> 'required|numeric',
			'confirm_password' => 'required|same:password',
			'address'	=> 'required',
			'contact'	=> 'required',
		];

		return $rules;
	}

	public function message()
	{
		return [];
	}
}