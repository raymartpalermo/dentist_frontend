<?php

namespace App\Laravel\Request\Customer\System;

use Illuminate\Foundation\Http\FormRequest;


use Session;

class PatientInfoRequest extends FormRequest
{
	public function rules()
	{
		$rules = [
			
			'firstname' => "required",
			'lastname'	=> "required",
			'contact' => "required",
			'email'	=> 'email|required',
			'date'	=> 'required|date',
		];

		return $rules;
	}

	public function messages()
	{
		return  [
			'firstname.required' => "First Name is Required",
			'lastname.required'	=> "Last Name is Required",
			'contact.required'	=> " Contact is Required",
			'email.email'	=> "Email is invalid",
			'email.required' => 'Email is Required',
			'date.required'	=> "Date is required",
			'date.date'	=> "Date is invalid",
		];
	}
}