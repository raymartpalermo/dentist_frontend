<?php

namespace App\Laravel\Request\Customer\System;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
	public function rules()
	{
		$rules = [
			'email' => 'required|email',
			'password' => 'required',
		];

		return $rules;
	}

	public function messages()
	{
		return [
			'email.required' => "Email is Required",
			'email.email'	=> "Email is invalid",
			'password.required'	=> "Password is Required",
		];
	}
}