<?php

namespace App\Laravel\Controllers\System;

use App\Http\Controllers\Controller;
use App\Laravel\Models\User;

class ProfileController extends Controller
{
	protected $data;
	
	public function __construct()
	{
		$this->data = [];
	}

	public function index()
	{
		return view('System.profile.index');
	}

}