<?php

namespace App\Laravel\Controllers\System;

use App\Http\Controllers\Controller;
use App\Laravel\Traits\checkImage;
use App\Laravel\Models\User;
use App\Laravel\Models\WebInformation;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
	use checkImage;

	protected $data;
	
	public function __construct()
	{
		$this->data = [];
					
	}

	public function index()
	{
		$this->data['settings'] =  WebInformation::Find(1);
		
		return view('System.settings.index',$this->data);
	}

	public function update(Request $request)
	{
		$settings = WebInformation::Find(1);

		if(!$settings)
		//Check if data is existing already
		{
		
			$settings = new webinformation();
			
			request()->imageCheck($settings);
			$settings->address = request('address');
			$settings->about_us = request('about_us');
			$settings->banner_text = request('banner');

			$settings->save();

			return rediect()->back()->with('success',"successfully updated!");	
		}
		else
		{
			$settings = WebInformation::Find(1);

			request()->imageCheck($settings);

			$settings->address = request('address');
			$settings->about_us = request('about_us');
			$settings->banner_text = request('banner');

			$settings->save();

			return redirect()->back()->with('success',"successfully updated!");
		}
	}

}