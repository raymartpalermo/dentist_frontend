<?php

namespace App\Laravel\Controllers\System;

use App\Http\Controllers\Controller;
use App\Laravel\Model\User;
use App\Laravel\Request\Admin\System\LoginRequest;
use Auth;

class AdminloginController extends Controller
{
	protected $data;
	
	public function __construct()
	{
		$this->data = [];
	}

	public function index()
	{
		return view('System.login.index');
	}

	public function login(LoginRequest $request)
	{
		$this->data['credentials'] =  array(
			'email' => request('email'),
			'password' => request('password'),
		);

		if(Auth::attempt($this->data['credentials']))
		{
			if(Auth::user()->type != "doctor")
			{
				Auth::logout();
				return back()->with("error",'Wrong type of user');
			}
			else
			return redirect()->route('system.dashboard');
		}
		else
		{	
			return back()->with('error',"Wrong Login Credentials!");
		}
	}

	public function logout()
	{
		Auth::logout();
		return redirect('/');
	}

}