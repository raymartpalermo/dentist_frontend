<?php

namespace App\Laravel\Controllers\System;

use App\Laravel\Controllers\System\Controller;
use App\Laravel\Notification\AppointmentApproval;
use App\Laravel\Models\{User,appointment,Schedule};
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;

class AppointmentsController extends Controller
{
	protected $data;

	public function __construct()
	{
		$this->data = [];
		$this->data['title'] = 'Appointments';
		$this->data['today'] = Carbon::now()->toDateString();

	}

	public function index()
	{
		$this->data['patients'] = appointment::where('approve','=','1')
								->whereDate('date','>=',$this->data['today'])
								->get();
		return view('System.appointments.index',$this->data);
	}

}