<?php

namespace App\Laravel\Controllers\System;

use App\Laravel\Controllers\System\Controller;
use App\Laravel\Notification\AppointmentApproval;
use App\Laravel\Models\{User,appointment,Schedule};
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;

class PendingAppointmentsController extends Controller
{

	protected $data;
	public function __construct()
	{
		$this->data = [];
	}
	public function index()
	{
		$today = Carbon::today()->toDateString();
		$this->data['patients'] = appointment::whereDate('date', '>=', $today)
								->where('approve','=','0')
								->get();

		return view('System.pending-appointments.index',$this->data);

	}

	public function update()
	{
		$this->data['patient'] = appointment::FindOrFail(request('id'));

		if($this->data['patient']->CheckSchedule())
		{
			Notification::send($this->data['patient'], new AppointmentApproval($this->data['patient']->date));
			return back()->with('success',"Appoinment accepted");
	}
		else
			return back()->with("error","Scheduled for this(".$this->data['patient']->date.")is full");
	}

}