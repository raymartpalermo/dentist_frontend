<?php

namespace App\Laravel\Controllers\System;

use App\Http\Controllers\Controller;
use App\Laravel\Notification\AppointmentApproval;
use App\Laravel\Models\User;
use Illuminate\Support\Facades\Notification;

class DashboardController extends Controller
{
	protected $data;
	
	public function __construct()
	{
		$this->data = [];
	}

	public function index()
	{
		return view('System.dashboard.index');
	}

}