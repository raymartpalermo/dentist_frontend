<?php

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Request\Customer\System\PatientInfoRequest;
use App\Laravel\Models\WebInformation;
use App\Laravel\Models\Appointment;
use Auth;


class CustappointController extends Controller
{

	public function __construct()
	{
		$this->data = [];
		$this->data['webinfo'] = WebInformation::Find(1);

	}

	public function index()
	{
		$this->data['patient_info'] = session()->pull('patient.info');

		return view('Frontend.confirm-appointment.index',$this->data);
	}

	public function store(PatientInfoRequest $request)
	{

		$appointment = new appointment();

		$appointment->firstname = $request->firstname;
		$appointment->lastname = $request->lastname;
		$appointment->date = $request->date;
		$appointment->email = $request->email;
		$appointment->contact = $request->contact;
		$appointment->save();

		return back()->with("success","Appointment success!");
	}

}