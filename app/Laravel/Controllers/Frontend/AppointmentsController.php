<?php

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\WebInformation;
use Session;

class AppointmentsController extends Controller
{

	public function __construct()
	{
		$this->data = [];
		$this->data['webinfo'] = WebInformation::Find(1);

	}
	public function index()
	{
		return view('Admin.System.Frontend.appointments',$this->data);
	}
}