<?php

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Request\Customer\System\PatientSignUp;
use App\Laravel\Models\{User,WebInformation};



class SignupController extends Controller
{
	
	public function __construct()
	{
		$this->data = [];
		$this->data['webinfo'] = WebInformation::Find(1);

	}

	public function index()
	{
		return view('Frontend.sign-up.index',$this->data);
	}

	public function store(PatientSignUp $request)
	{
		// dd(request()->all());
		$user = new user();	

		$user->firstname = request('firstname');
		$user->lastname	= request('lastname');
		$user->age = request('age');
		$user->email = request('email');
		$user->contact = request('contact');
		$user->address = request('address');
		$user->password = bcrypt(request('password'));
		$user->type = "patient";

		$user->save();

		return redirect()->route('frontend.login')->with('success', "Sucessfully Regitered!");
	}
}