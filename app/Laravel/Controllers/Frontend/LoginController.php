<?php

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Request\Customer\System\LoginRequest;
use App\Laravel\Models\WebInformation;
use Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
	public function __construct()
	{
		$this->data = [];
		$this->data['webinfo'] = WebInformation::Find(1);

	}

	public function index()
	{
		return view('Frontend.login.index',$this->data);
	}

	public function login(LoginRequest $request)
	{
		$this->data['credentials'] = array(
			'email'	=> request('email'),
			'password' => request('password'),
			'type' => "patient",
		);

		if(Auth::attempt($this->data['credentials']))
		{
			return redirect()->route('Frontend.confirm-appointment')->with('success',"Successfully Login");
		}
		else
		{
			return back()->with('error',"Wrong Login Credentials");
		}
	}

	public function logout()
	{
		Auth::logout();

		return redirect()->route('frontend.home');
	}
}