<?php

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Request\Customer\System\PatientInfoRequest;
use App\Laravel\Models\WebInformation;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;

class HomepageController extends Controller
{

	public function __construct()
	{
		$this->data = []; 

		$this->data['webinfo'] = WebInformation::Find(1);
	}

	public function index()
	{
		return view('frontend.home.index',$this->data);
	}

	public function info(PatientInfoRequest $request)
	{
		session()->push('patient.info', $request->all());
		// dd(session()->get('patient.info'));


		if(!Auth::user())
			return redirect()->route('frontend.login');
		else
			return redirect()->route('frontend.confirm_appointment');
	}

}