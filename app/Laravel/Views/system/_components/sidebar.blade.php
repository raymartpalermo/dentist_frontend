			<!-- Sidebar -->
            <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
					<div id="sidebar-menu" class="sidebar-menu">
						<ul>
							<li class="menu-title"> 
								<span>Main</span>
							</li>
							<li class="active"> 
								<a href="{{route('system.dashboard')}}"><i class="fe fe-home"></i> <span>Dashboard</span></a>
							</li>
							<li> 
								<a href="{{route('system.pending_appointments')}}"><i class="fe fe-layout"></i> <span>Pending Appointments</span></a>
							</li>
							<li> 
								<a href="{{route('system.appointments')}}"><i class="fe fe-layout"></i> <span> Appointments</span></a>
							</li>
							<li> 
								<a href="{{route('system.profile')}}"><i class="fe fe-user-plus"></i> <span>Profile</span></a>
							</li>
							<li> 
								<a href="{{route('system.settings')}}"><i class="fe fe-user-plus"></i> <span>Settings</span></a>
							</li>
						</ul>
					</div>
                </div>
            </div>
			<!-- /Sidebar -->