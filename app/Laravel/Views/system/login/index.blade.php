@extends('System._layouts.login')
@section('content')

<!-- Main Wrapper -->
<div class="main-wrapper login-body">
            <div class="login-wrapper">
            	<div class="container">
                	<div class="loginbox">
                    	<div class="login-left">
							<img class="img-fluid" src="{{asset('admin/assets/img/logo-white.png')}}" alt="Logo">
                        </div>
                        <div class="login-right">
							<div class="login-right-wrap">
								<h1>Login</h1>
								<p class="account-subtitle">Access to our dashboard</p>
								<!-- Form -->
								@if(session()->has('error'))
									<div class="alert alert-danger">
										<li>{{session('error')}}</li>
									</div>
								@endif
								<form method="POST">
									@csrf
									<div class="form-group">
										<input name="email" id="email" class="form-control {{$errors->has('email') ? "is-invalid" : ""}}" type="text" placeholder="Email">
										@error('email')
											<div class="invalid-feedback">{{$message}}</div>
										@enderror
									</div>
									<div class="form-group">
										<input name="password" id="passwod" class="form-control {{$errors->has('password') ? "is-invalid" : ""}}" type="password" placeholder="Password">
										@error('password')
											<div class="invalid-feedback">{{$message}}</div>
										@enderror
									</div>
									<div class="form-group">
										<button class="btn btn-primary btn-block" type="submit">Login</button>
									</div>
								</form>
								<!-- /Form -->
								
								<div class="text-center forgotpass"><a href="forgot-password.html">Forgot Password?</a></div>
								
							</div>
                        </div>
                    </div>
                </div>
            </div>
</div>
<!-- /Main Wrapper -->