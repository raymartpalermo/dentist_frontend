@extends('System._layouts.main')
@section('content')

<!-- Page Wrapper -->
<div class="page-wrapper">
	<div class="content container-fluid">
		
		<!-- Page Header -->
		<div class="page-header">
			<div class="row">
				<div class="col">
					<h3 class="page-title">Settings</h3>
					<ul class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
						<li class="breadcrumb-item active">Settings</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- /Page Header -->
		<form action="{{route('system.settings.update')}}" method="POST" enctype="multipart/form-data">
			@csrf
			@method('PATCH')
			<div class="row">
				<div class="col-md-12">
					<div class="tab-content profile-tab-cont">
						<!-- Personal Details Tab -->
						<div class="tab-pane fade show active" id="per_details_tab">
							<!-- Personal Details -->a
							<div class="row">
								<div class="col-lg-12">
									<div class="card">
										<div class="card-body">
											@if(session()->has('success'))
												<div class="alert alert-success col-lg-12">
													{{session('success')}}
												</div>
											@endif
											<h5 class="card-title d-flex justify-content-between">
											<span>Header Form</span>
											</h5>
											<hr>
											<div class="form-group row">
												<label class="col-form-label col-md-2">Address</label>
												<div class="col-md-10">
													<input value="{{$settings->address}}" type="text" name="address" id="address" class="form-control">
												</div>
											</div>
											<div class="form-group row">
												<label class="col-form-label col-md-2">Logo Input</label>
												<div class="col-md-10">
													<input class="form-control" name="logo"	id="logo" type="file">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Personal Details -->
						</div>
						<!-- /Personal Details Tab -->
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="tab-content profile-tab-cont">
						<!-- Personal Details Tab -->
						<div class="tab-pane fade show active" id="per_details_tab">
							<!-- Personal Details -->
							<div class="row">
								<div class="col-lg-12">
									<div class="card">
										<div class="card-body">
											<h5 class="card-title d-flex justify-content-between">
											<span>Banner Form</span>
											</h5>
											<hr>
											<div class="form-group row">
												<label class="col-form-label col-md-2">Banner Text</label>
												<div class="col-md-10">
													<textarea name="banner" id="banner" rows="5" cols="5" class="form-control" placeholder="Enter text here">{{$settings->banner_text}}</textarea>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-form-label col-md-2">Image Input</label>
												<div class="col-md-10">
													<input type="file" name="banner_image" id="banner_image" class="form-control">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Personal Details -->
						</div>
						<!-- /Personal Details Tab -->
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="tab-content profile-tab-cont">
						<!-- Personal Details Tab -->
						<div class="tab-pane fade show active" id="per_details_tab">
							<!-- Personal Details -->
							<div class="row">
								<div class="col-lg-12">
									<div class="card">
										<div class="card-body">
											<h5 class="card-title d-flex justify-content-between">
											<span>About Us Form</span>
											</h5>
											<hr>
											<div class="form-group row">
												<label class="col-form-label col-md-2">About Us Text</label>
												<div class="col-md-10">
													<textarea name="about_us" id="about_us" rows="5" cols="5" class="form-control" placeholder="Enter text here">{{$settings->about_us}}</textarea>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-form-label col-md-2">Image Input</label>
												<div class="col-md-10">
													<input name="about_us_image" id="about_us_image" class="form-control" type="file">
												</div>
											</div>
											<div class="col-lg-12">
												<button type="Submit" class="btn btn-primary pull-right">Submit</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Personal Details -->
						</div>
						<!-- /Personal Details Tab -->
					</div>
				</div>
			</div>
		</form>

{{-- 		<div class="row">
			<div class="col-md-12">
				<div class="tab-content profile-tab-cont">
					<!-- Personal Details Tab -->
					<div class="tab-pane fade show active" id="per_details_tab">
						<!-- Personal Details -->
						<div class="row">
							<div class="col-lg-12">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title d-flex justify-content-between">
										<span>Best Services Form</span>
										</h5>
										<hr>
										<h5 class="card-title d-flex justify-content-between">
											<span></span>
										<a class="edit-link" data-toggle="modal" href="#edit_personal_details"><i class="fa fa-plus mr-1"></i>Add new</a>
										</h5>
										<div class="form-group row">
											<label class="col-form-label col-md-2">Logo Input</label>
											<div class="col-md-10">
												<input class="form-control" type="file">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-md-2">Title Text</label>
											<div class="col-md-10">
												<input type="text" class="form-control">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-md-2">Information Text</label>
											<div class="col-md-10">
												<textarea rows="5" cols="5" class="form-control" placeholder="Enter text here"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /Personal Details -->
					</div>
					<!-- /Personal Details Tab -->
				</div>
			</div>
		</div>	

		<div class="row">
			<div class="col-md-12">
				<div class="tab-content profile-tab-cont">
					<!-- Personal Details Tab -->
					<div class="tab-pane fade show active" id="per_details_tab">
						<!-- Personal Details -->
						<div class="row">
							<div class="col-lg-12">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title d-flex justify-content-between">
										<span>Our Team Form</span>
										</h5>
										<hr>
										<h5 class="card-title d-flex justify-content-between">
											<span></span>
											<a class="edit-link" data-toggle="modal" href="#edit_personal_details"><i class="fa fa-plus mr-1"></i>Add new</a>
										</h5>
										<div class="form-group row">
											<label class="col-form-label col-md-2">Image Input</label>
											<div class="col-md-10">
												<input class="form-control" type="file">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-md-2">Name Text</label>
											<div class="col-md-10">
												<input type="text" class="form-control">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-md-2">Position Text</label>
											<div class="col-md-10">
												<input type="text" class="form-control">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /Personal Details -->
					</div>
					<!-- /Personal Details Tab -->
				</div>
			</div>
		</div>	

		<div class="row">
			<div class="col-md-12">
				<div class="tab-content profile-tab-cont">
					<!-- Personal Details Tab -->
					<div class="tab-pane fade show active" id="per_details_tab">
						<!-- Personal Details -->
						<div class="row">
							<div class="col-lg-12">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title d-flex justify-content-between">
										<span>Why Choose Us Form</span>
										</h5>
										<hr>
										<div class="form-group row">
											<label class="col-form-label col-md-2">Image Input</label>
											<div class="col-md-10">
												<input class="form-control" type="file">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-md-2">Title Text</label>
											<div class="col-md-10">
												<input type="text" class="form-control">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-md-2">Information Text</label>
											<div class="col-md-10">
												<textarea rows="5" cols="5" class="form-control" placeholder="Enter text here"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /Personal Details -->
					</div>
					<!-- /Personal Details Tab -->
				</div>
			</div>
		</div>	 --}}

	</div>
</div>
<!-- /Page Wrapper -->