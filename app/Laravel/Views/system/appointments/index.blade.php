@extends('System._layouts.main')
@section('content')

            <div class="page-wrapper">
                <div class="content container-fluid">
				
					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col-sm-12">
								<h3 class="page-title">Appointments</h3>
								<ul class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
									<li class="breadcrumb-item active">Appointments</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /Page Header -->
					<div class="row">
						<div class="col-md-12">
						
							<!-- Recent Orders -->
							<div class="card">
								<div class="card-body">
									<div class="table-responsive">
										<table id="patientTable" class="datatable table table-hover table-center mb-0">
											@if(session()->has("error"))
												<div class="alert alert-danger">
													{{session('error')}}
												</div>
											@elseif(session()->has('success'))
												<div class="alert alert-success">
													{{session('success')}}
												</div>
											@endif
											<thead>
												<tr>
													<th>Name</th>
													<th>Contact</th>
													<th>Date</th>
													<th>email</th>
												</tr>
											</thead>
											<tbody>
												@foreach($patients as $patient)
												<tr>
													<td>
														{{$patient->getFullNameAttribute()}}
													</td>
													<td>{{$patient->contact}}</td>
													<td>
														<h2 class="table-avatar">
															{{$patient->getFromDateAttribute()}}
														</h2>
													</td>
													<td>{{$patient->email}}</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>			
			</div>

			<!-- Modal Update -->
				<div class="modal" tabindex="-1" role="dialog" id="ApproveModal">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
							<form method="POST">
							@csrf
							@method('PATCH')
								<h5 class="modal-title">Approve patient appointment</h5>
								<button type="button" class="close" data-dismiss="modal" arial-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<input type="hidden" name="id" id="id">
								<p id="message"></p>
							</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-primary">Approve</button>
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						    </div>
							</form>
						</div>
					</div>
				</div>
			<!-- /Modal Update -->

@endsection
{{-- @section('scripts')

	<script>
		$(document).ready(function() {
			$('table').DataTable();
			});
			
			$('#ApproveModal').on('show.bs.modal',function(event){
				var button = $(event.relatedTarget);

				var patient_id = button.data('patient_id');
				var name = button.data('name');

				var modal = $(this);

				modal.find('.modal-body #message').text("Approve patient "+name+"?");
				modal.find('.modal-body #id').val(patient_id);
			});

	</script>

@endsection --}}