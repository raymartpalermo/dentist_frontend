<!DOCTYPE html>
<html lang="en">
    
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Oreo Hospital</title>
		@include('System._components.styles')
    	@yield('styles')
    </head>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
		
			<!-- Header -->
				@include('System._components.header')
			<!-- /Header -->
			
			<!-- Sidebar -->
				@include('System._components.sidebar')
			<!-- /Sidebar -->
			
			<!-- Content Area -->
			<section class="main-section">

			    @yield('content')

			</section>
		
        </div>
		<!-- /Main Wrapper -->
		   @include('System._components.scripts')
   			@yield('scripts')
		
    </body>
</html>