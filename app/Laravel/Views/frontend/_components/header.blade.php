<div class="main_header">
    <section id="top-nav">
        <div class="container">
            <div class="top">
                <div class="row">
                    <div class="col-lg-8 col-md-7"></div>
                        <div class="col-lg-4 col-md-5">
                            <div class="text-right d-none d-md-block">
                                <ul class="list-unstyled m-b-0">
                                    @if(!Auth()->user())
                                        <li>
                                            <a href="{{route('frontend.login')}}" class="btn btn-link">Login</a>
                                            <a href="{{route('frontend.signup')}}" class="btn-link">Sign Up</i></a>
                                        </li>
                                    @else
                                        <a href="{{route('frontend.logout')}}" class="btn btn-link">Logout</a>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <header id="header">
        <div class="container">
            <div class="head">
                <div class="row">
                    <div class="col-lg-5 col-sm-5">
                        <div class="left">
                            <a href="{{route('frontend.home')}}" class="navbar-brand"><img src="{{asset('/admin/assets/img/cms/'.$webinfo->getLogo().'')}}" alt="logo"></a>
                        </div>
                    </div>
                    <div class="col-lg-7 col-sm-7">
                        <div class="text-right d-none d-md-block">
                            <p class="col-white m-b-0 p-t-5"><i class="zmdi zmdi-time"></i> Mon - Sat: 9:00 - 18:00
                                Sunday CLOSED </p>
                            <p class="col-white m-b-0"><i class="zmdi zmdi-pin"></i> {{-- 1422 1st St. Santa Rosa CA
                                94559. United States --}} {{$webinfo->getAddress()}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
</div>
