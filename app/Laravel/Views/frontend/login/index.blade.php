@extends('Frontend._layouts.main')
@section('content')
	<div class="card col-lg-12">
		<div class="container">

			<div class="section-title col-12" data-aos="fade-right">
				<h2>Login</h2>
			</div>

			<div class="col-lg-12">

				{{-- Display Success message --}}
				@if(session()->has('success'))
					<div class="alert alert-success">
						<li>{{session('success')}}</li>
					</div> 
				@endif
				{{-- End Display Success message --}}

				{{-- display error message --}}
				@if(session()->has('error'))
					<div class="alert alert-danger">
						<li>{{session('error')}}</li>
					</div>
				@endif
				@if($errors->any())
					<div class="alert alert-danger">
						@foreach($errors->all() as $error)
							<li>{{$error}}</li>
						@endforeach
					</div>
				@endif
				{{-- end error message --}}

				{{-- Login Form --}}
				<form method="POST">
				@csrf
					<div class="row">
						<div class="col-sm-6">
						{{-- Email Field --}}
							<div class="form-group">
								<input type="text" name="email" id="email" placeholder="E-mail" class="form-control mb-3 
								{{$errors->has('email') ? 'is-invalid' : ''}}" value="{{old('email')}}">
								@error('email')<div class="invalid-feedback mb-3">{{$message}}</div>@enderror
						{{-- End Email Field --}}

						{{-- Password Field --}}
								<input type="password" name="password" id="password" placeholder="password" class="form-control
								{{$errors->has('password') ? 'is-invalid' : ''}}">
								@error('password')<div class="invalid-feedback mb-3">{{$message}}</div>@enderror
							</div>
						{{-- End Password Field --}}
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<div class="row">
								<button type="submit" class="btn btn-round btn-info">Sign In</button>
							</div>
							<div class="row">
								<a href="{{route('frontend.signup')}}">Don't Have an Account?</a>
							</div>
						</div>
					</div>
				</form>
				{{-- End Login Form --}}
			</div>
		</div>
	</div>
@endsection