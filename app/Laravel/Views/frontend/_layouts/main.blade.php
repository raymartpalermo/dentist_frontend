<!doctype html>
<html class="no-js " lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>:: Oreo Hospital :: Home</title>
    <link rel="icon" href="favicon.ico">
    <!-- start linking -->
    @include('Frontend._components.styles')
    @yield('styles')
</head>
<body>

<div id="loading" class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="{{asset('customer/assets/images/loader.svg')}}" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>

<div class="wrapper">
    <!-- start loading -->    
    @include('Frontend._components.header')
    
    <!-- Content Area -->
    <section class="main-section">

       @yield('content')

    </section>

    <!-- start footer -->
   @include("Frontend._components.footer")
</div>
<!-- start screpting -->
   @include('Frontend._components.scripts')
   @yield('scripts')
</body>
</html>
