@extends('Frontend._layouts.main')

@section('content')
	<div class="sign-up-section">
		<div class="section-title" data-aos="fade-right">
			<h2>Sign Up</h2>
		</div>

		{{-- Sign-Up Form --}}
		<form method="POST">
			@csrf
			<div class="row justify-content-center">
				<div class="col-lg-10">
					<div class="card">
						{{-- Error Field --}}
						@if($errors->any())
							<div class="alert alert-danger">
								@foreach($errors->all() as $error)
									<li>{{$error}}</li>
								@endforeach
							</div>
						@endif
						{{-- End Error Field --}}
						<div class="row">

							{{-- Firstname Field --}}
							<div class="col-lg-4 col-sm-6">
								<div class="form-group">
									<input type="text" name="firstname" id="firstname" placeholder="First Name" class="form-control 
									{{$errors->has('firstname') ? 'is-invalid' : ''}} value="{{old('firstname')}}">
									@error('firstname')
										<div class="invalid-feedback">{{$message}}</div>
									@enderror
								</div>
							</div>
							{{-- End Firstname Field --}}

							<div class="col-lg-4 col-sm-6">
								<div class="form-group">
									<input type="text" name="lastname" id="lastname" placeholder="Last Name" class="form-control
									{{$errors->has('lastname') ? 'is-invalid' : ''}}">
									@error('lastname')
										<div class="invalid-feedback">{{$message}}</div>
									@enderror
								</div>
							</div>
							<div class="col-lg-4 col-sm-6">
								<div class="form-group">
									<input type="email" name="email" id="email" placeholder="E-mail" class="form-control
									{{$errors->has('email') ? 'is-invalid' : ''}}">
									@error('email')
										<div class="invalid-feedback">{{$message}}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4 col-sm-6">
								<div class="form-group">
									<input type="text" name="contact" id="contact" placeholder="Contact Number" class="form-control
									{{$errors->has('contact') ? 'is-invalid' : ''}}">
									@error('contact')
									<div class="invalid-feedback">{{$message}}</div>
								@enderror
								</div>
							</div>
							<div class="col-lg-4 col-lg-4">
								<div class="form-group">
									<input type="password" name="password" id="password" placeholder="Password" class="form-control
										{{$errors->has('password') ? 'is-invalid' : ''}}">
										@error('password')
											<div class="invalid-feedback">{{$message}}</div>
										@enderror
									</div>
							</div>
							<div class="col-lg-4 col-sm-4">
								<div class="form-group">
									<input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password" class="form-control {{$errors->has('confirm_password') ? 'is-invalid' : ''}}">
									@error('confirm_password')
										<div class="invalid-feedback">{{$message}}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4 col-sm-6">
								<div class="form-group">
									<input type="number" name="age" id="age" placeholder="Age" class="form-control 
									{{$errors->has('age') ? 'is-invalid' : ''}}">
									@error('age')
										<div class="invalid-feedback">{{$message}}</div>
									@enderror
								</div>
							</div>
							<div class="col-sm-8">
								<div class="form-group">
									<textarea name="address" id="address" placeholder="Address" class="form-control 
									{{$errors->has('address') ? 'is-invalid' : ''}}" rows="1"></textarea>
									@error('address')
										<div class="invalid-feedback">{{$message}}</div>
									@enderror
								</div>
							</div>
						</div>
						<button type="submit" class="btn btn-round btn-primary float-right">Submit</button>
					</div>
				</div>
			</div>
		</form>
		{{-- End sign-up Form --}}
	</div>
@endsection