<?php

namespace App\Laravel\Middleware;

use Closure;
use Auth;
use App\Laravel\User;

class CheckType
{
	public function handle($request, Closure $next,$type)
	{
		$user = Auth::user();
		if($user->type == $type)
		{
			return $next($request);
		}
		abort(403,'Unauthorized Action!');
	}
}