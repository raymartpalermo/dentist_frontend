<?php
Route::group(['namespace' => 'Frontend','as' => 'frontend.'],function(){

	//Home Page 
		Route::get('/',['as' => "home", 'uses' => 'HomepageController@index']);
		Route::post('/',['uses' => 'HomepageController@info']);

	//Guest Middleware
		Route::group(['middleware' => 'guest'],function(){
	
		//Login Page
			Route::get('login',['as' => "login", 'uses' => "LoginController@index"]);
			Route::post('login',['uses' => "LoginController@login"]);

		//Appointment
			Route::get('appointment',['as' => "appointment", 'uses' => 'AppointmentsController@index']);

		//Sign-Up
			Route::get('sign-up',['as' => "signup", 'uses' => "SignupController@index"]);
			Route::post('sign-up',['uses' => "SignupController@store"]);
		
		});
	//End Guest Middleware
		
	//Auth patient middleware
		Route::group(['middleware' => ['auth','type:patient']],function(){

		//Confirmation_Appointment
			Route::get('confirm-appointment',['as' => "confirm_appointment", 'uses' => "CustappointController@index"]);
			Route::post('confirm-appointment',['uses' => "CustappointController@store"]);
			
		//Logout
			Route::get('logout',['as' => "logout", 'uses' => "LoginController@logout"]);
		
		});
	//End Auth patient middleware
});


