<?php

Route::group([

	'namespace' => "System",
	'as' => 'system.',
	'prefix' => 'doctor',

],function(){



	//Admin
	Route::group([
		'middleware' => 'guest'
	],function(){

		Route::get('',['as' => "adminlogin", 'uses' => "AdminloginController@index"]);
		Route::post('',['uses' => "AdminloginController@login"]); 

	});
		

	Route::group(['middleware' => ['auth','type:doctor']],function(){

		//Dashboard Page
			Route::get('/dashboard',['as' => "dashboard", 'uses' => "DashboardController@index"]);
			Route::get('/logout',['as' => 'logout', 'uses' => "AdminloginController@logout"]);	
		
		//Pending Appointments
			Route::get('/pending-appointments',['as' => "pending_appointments",'uses' => "PendingAppointmentsController@index"]);
			Route::patch('/pending-appointments',['uses' => "PendingAppointmentsController@update"]);

		//Appointments
			Route::get('/appointments',['as' => "appointments", 'uses' => "AppointmentsController@index"]);
			Route::patch('/appointments',['uses' => "AppointmentsController@update"]);

		//Profile
			Route::get('/profile',['as' => "profile", 'uses' => "ProfileController@index"]); 

		//Settings
			Route::get('/settings',['as' => "settings", 'uses' => "SettingsController@index"]); 	
			Route::patch('/settings',['as' => "settings.update",'uses' => "SettingsController@update"]);
	});
	
});