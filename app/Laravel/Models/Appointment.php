<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Schedule;
use App\Laravel\Traits\ApproveTrait;
use Illuminate\Notifications\Notifiable;



class Appointment extends Model
{
    use ApproveTrait,Notifiable;
    
    protected $table = "appointments";

    protected $fillable = [
    	"firstname","lastname","date","email","approved","contact"
    ];

    public function getFullNameAttribute()
    {
        return "{$this->firstname} {$this->lastname}";
    }

    public function getFromDateAttribute()
    {
	    return \Carbon\Carbon::parse($this->date)->format('M-d-Y');
	}

}
