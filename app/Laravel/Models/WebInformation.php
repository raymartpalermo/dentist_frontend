<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;


class WebInformation extends Model
{
	protected $table = '_web_information';


	public function getAddress(){
		
		return $this->address;

	}

	public function getAboutUs()
	{
		return $this->about_us;
	}

	public function getAboutImg()
	{
		return $this->about_us_image;
	}

	public function getBannerImg()
	{
		return $this->banner_image;
	}

	public function getLogo()
	{
		return $this->logo;
	}

}