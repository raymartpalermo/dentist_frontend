<?php

namespace App\Laravel\Traits;

use Illuminate\Http\Request;
use File;

trait CheckImage
{
	public function imageCheck($settings)
	{
		if($this->hasFile('about_us_image'))
		{
			$image = request('about_us_image');
			$imageAbout = time()."_about.".$image->getClientOriginalExtension();
			$path = public_path('/admin/assets/img/cms');
			$image->move($path,$imageAbout);
			File::delete($path.'/'.$settings->about_us_image);
			$settings->about_us_image = $imageAbout;
		}

		if($this->hasFile('banner_image'))
		{
			$image = request('banner_image');
			$imageBanner = time()."_banner.".$image->getClientOriginalExtension();
			$path = public_path('/admin/assets/img/cms');
			$image->move($path,$imageBanner);
			File::delete(public_path($path.'/'.$settings->banner_image.''));
			$settings->banner_image = $imageBanner;
		}

		if($this->hasFile('logo'))
		{
			$image = request('logo');
			$imageLogo = time()."_logo.".$image->getClientOriginalExtension();
			$path = public_path('/admin/assets/img/cms');
			$image->move($path,$imageLogo);
			File::delete($path.'/'.$settings->logo);
			$settings->logo = $imageLogo;
		}

	}
}