<?php

namespace  App\Laravel\Traits;

use App\Laravel\{appointment,Schedule};


trait ApproveTrait
{
	public function CheckSchedule()
	{
		$scheduled = Schedule::where('date',$this->date)->count();

		if($scheduled >= 0 && $scheduled < 5)
		{	
			//Create new Schedule
			$schedule = new schedule();

			$schedule->date = $this->date;
			$schedule->save();

			//Update Patient Info
			$this->approve = 1;
			$this->save();
			
			return true;	
		}
		else
		{
			return false;
		}
	}

}